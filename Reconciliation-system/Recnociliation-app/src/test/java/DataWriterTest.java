import com.progressoft.implment.*;
import com.progressoft.interfaces.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataWriterTest {
    DataWriter dataWriter;

    @Test
    public void canCreate() {
        dataWriter = new DataWriter(null);
    }


    @Test
    public void givenMap_whenConstruct_thenFail() {
        Map<String, List<Transaction>> listMap = null;
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> new DataWriter(null).writeAllDataToFiles(listMap));
        Assertions.assertEquals("invalid map", exception.getMessage());
    }

    @Test
    public void givenAnEmptyMap_whenConstruct_thenFail() {
        Map<String, List<Transaction>> listMap = new HashMap<>();
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> new DataWriter(null).writeAllDataToFiles(listMap));
        Assertions.assertEquals("map was empty", exception.getMessage());
    }

    @Test
    public void givenValidMap_whenConstructThen_thenSuccess() {
        DefaultResourceReader csvReader = new DefaultResourceReader();
        DefaultResourceReader jsonReader = new DefaultResourceReader();
        csvReader.setReadPath("src/main/resources/bank-transactions.csv");
        jsonReader.setReadPath("src/main/resources/online-banking-transactions.json");
        CsvDataParserImpl csvDataParser = new CsvDataParserImpl();
        JsonDataParserImpl jsonDataParser = new JsonDataParserImpl();
        List<Transaction> jsonList = jsonDataParser.parseData(jsonReader, FileOrigin.TARGET);
        List<Transaction> csvList = csvDataParser.parseData(csvReader, FileOrigin.SOURCE);
        DataMatcher matcher = new DataMatcher();

        Map<String, List<Transaction>> map = matcher.getAllData(csvList, jsonList);
        dataWriter = new DataWriter(null);
        String d = System.getProperty("user.home");

        String dirFile = d + File.separator + "Documents" + File.separator + "Reconciliation-system results" + File.separator + "matching-transactions.csv";
        Path path = Paths.get(dirFile);
        Assertions.assertTrue(Files.exists(path));
        Assertions.assertTrue(new File(String.valueOf(path)).length() > 0);
        dirFile = d + File.separator + "Documents" + File.separator + "Reconciliation-system results" + File.separator + "mismatched-transactions.csv";
        path = Paths.get(dirFile);
        Assertions.assertTrue(Files.exists(path));
        Assertions.assertTrue(new File(String.valueOf(path)).length() > 0);
        dirFile = d + File.separator + "Documents" + File.separator + "Reconciliation-system results" + File.separator + "missing-transactions.csv";
        path = Paths.get(dirFile);
        Assertions.assertTrue(Files.exists(path));
        Assertions.assertTrue(new File(String.valueOf(path)).length() > 0);


    }
}
