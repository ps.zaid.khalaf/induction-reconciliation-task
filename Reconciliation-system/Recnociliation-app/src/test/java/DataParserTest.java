import com.progressoft.implment.*;
import com.progressoft.interfaces.DataParser;
import com.progressoft.interfaces.ResourceReader;
import com.progressoft.interfaces.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

class DataParserTest {
    DataParser parser;
    static final String JSON_FILE_PATH = "src/main/resources/online-banking-transactions.json";
    static final String CSV_FILE_PATH = "src/main/resources/bank-transactions.csv";

    @Test
    public void canCreate() {
        parser = new CsvDataParserImpl();

    }

    @Test
    public void givenInvalidResourceReader_whenParseData_thenFail() {
        parser = new CsvDataParserImpl();

        IllegalArgumentException EmptyFile = Assertions.assertThrows(IllegalArgumentException.class,
                () -> parser.parseData(null, null));
        Assertions.assertEquals("invalid reader was inserted or file path was empty", EmptyFile.getMessage());
    }

    @Test
    public void whenValidResourceReader_givenParseData_thenSuccess() {
        parser = new CsvDataParserImpl();
        ResourceReader reader = new DefaultResourceReader();
        reader.setReadPath(CSV_FILE_PATH);
        List<Transaction> transactionList = parser.parseData(reader, FileOrigin.SOURCE);
        TransactionImpl transaction = new TransactionImpl().setReference("TR-47884222201")
                .setAmount(new BigDecimal("140.00")).setCurrencyCode("USD")
                .setDate("2020/01/20");
        Assertions.assertEquals(transactionList.get(0), transaction, "not the same");

        reader.setReadPath(JSON_FILE_PATH);
        parser = new JsonDataParserImpl();
        transactionList = parser.parseData(reader, FileOrigin.TARGET);
        transaction = new TransactionImpl()
                .setReference("TR-47884222201").setDate("2020/01/20")
                .setAmount(new BigDecimal("140.00"))
                .setCurrencyCode("USD");
        Assertions.assertEquals(transactionList.get(0), transaction, "not the same");

    }

}