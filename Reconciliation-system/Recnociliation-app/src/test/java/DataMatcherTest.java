import com.progressoft.implment.*;
import com.progressoft.interfaces.DataParser;
import com.progressoft.interfaces.ResourceReader;
import com.progressoft.interfaces.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DataMatcherTest {
    DataParser csvParser = new CsvDataParserImpl();
    DataParser jsonParser = new JsonDataParserImpl();
    ResourceReader reader = new DefaultResourceReader();

    @Test
    public void canCreate() {
        List<Transaction> transactions = new ArrayList<>() {{
            add(new TransactionImpl()
                    .setReference("TR-47884222206")
                    .setAmount(new BigDecimal("500.00"))
                    .setCurrencyCode("USD")
                    .setDate("2020/02/10"));
        }};
        List<Transaction> transactions1 = new ArrayList<>() {{
            add(new TransactionImpl()
                    .setReference("TR-47884222206")
                    .setAmount(new BigDecimal("500.00"))
                    .setCurrencyCode("USD")
                    .setDate("2020/02/10"));
        }};
        new DataMatcher();
    }

    @Test
    public void givenInvalidParsers_whenConstruct_thenFail() {
        List<Transaction> transactionList1 = null;

        List<Transaction> transactionList2 = null;

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new DataMatcher().getAllData(transactionList1, transactionList2));
        Assertions.assertEquals(exception.getMessage(), "invalid lists");
//
//        List<Transaction> transactionList3 = new ArrayList<>();
//        List<Transaction> transactionList4 = new ArrayList<>();
//        IllegalArgumentException exception2 = Assertions.assertThrows(IllegalArgumentException.class, () -> new DataMatcher(transactionList3, transactionList4));
//        Assertions.assertEquals(exception2.getMessage(), "one or both lists are empty");
    }

    @Test
    @Disabled
    public void givenValidLists_WhenMatchData_thenSuccess() {
        reader.setReadPath("src/main/resources/bank-transactions.csv");
        List<Transaction> csvList = csvParser.parseData(reader, FileOrigin.SOURCE);
        reader.setReadPath("src/main/resources/online-banking-transactions.json");
        List<Transaction> jsonList = jsonParser.parseData(reader, FileOrigin.TARGET);
        Assertions.assertFalse(csvList.isEmpty());
        Assertions.assertFalse(jsonList.isEmpty());
        DataMatcher matcher = new DataMatcher();
        Map<String, List<Transaction>> actualMap = matcher.getAllData(csvList, jsonList);
        List<Transaction> actualMatchingList = actualMap.get("match data");
        List<Transaction> expectedMatchingList = new ArrayList<>() {
            {
                add(new TransactionImpl()
                        .setReference("TR-47884222201")
                        .setAmount(new BigDecimal("140.00"))
                        .setCurrencyCode("USD")
                        .setDate("2020/01/20"));
                add(new TransactionImpl()
                        .setReference("TR-47884222203")
                        .setAmount(new BigDecimal("5000.000"))
                        .setCurrencyCode("JOD")
                        .setDate("2020/01/25"));
                add(new TransactionImpl()
                        .setReference("TR-47884222206")
                        .setAmount(new BigDecimal("500.00"))
                        .setCurrencyCode("USD")
                        .setDate("2020/02/10"));
            }
        };
        Assertions.assertEquals(expectedMatchingList, actualMatchingList);
        List<Transaction> expectedMismatchList = new ArrayList<>() {
            {
                add(new TransactionImpl()
                        .setReference("TR-47884222202")
                        .setAmount(new BigDecimal("20.000"))
                        .setCurrencyCode("JOD")
                        .setDate("2020/01/22"));
                add(new TransactionImpl()
                        .setReference("TR-47884222202")
                        .setAmount(new BigDecimal("30.000"))
                        .setCurrencyCode("JOD")
                        .setDate("2020/01/22"));
                add(new TransactionImpl()
                        .setReference("TR-47884222205")
                        .setAmount(new BigDecimal("60.000"))
                        .setCurrencyCode("JOD")
                        .setDate("2020/02/02"));

                add(new TransactionImpl()
                        .setReference("TR-47884222205")
                        .setAmount(new BigDecimal("60.000"))
                        .setCurrencyCode("JOD")
                        .setDate("2020/02/03"));
            }
        };
        List<Transaction> actualMisMatchList = actualMap.get("mismatch data");
        Assertions.assertEquals(expectedMismatchList, actualMisMatchList);
        List<Transaction> expectedMissingList = new ArrayList<>() {
            {
                add(new TransactionImpl()
                        .setReference("TR-47884222204")
                        .setAmount(new BigDecimal("1200.000"))
                        .setCurrencyCode("JOD")
                        .setDate("2020/01/31"));
                add(new TransactionImpl()
                        .setReference("TR-47884222217")
                        .setAmount(new BigDecimal("12000.000"))
                        .setCurrencyCode("JOD")
                        .setDate("2020/02/14"));
                add(new TransactionImpl()
                        .setReference("TR-47884222245")
                        .setAmount(new BigDecimal("420.00"))
                        .setCurrencyCode("USD")
                        .setDate("2020/01/12"));
            }
        };
        List<Transaction> actualMissingList = actualMap.get("missing data");
        Assertions.assertEquals(expectedMissingList, actualMissingList);
    }


}
