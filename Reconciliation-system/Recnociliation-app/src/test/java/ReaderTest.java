import com.progressoft.implment.DefaultResourceReader;
import com.progressoft.interfaces.ResourceReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ReaderTest {
    ResourceReader reader;

    @Test
    public void canCreate() {
        new DefaultResourceReader();
    }

    @BeforeEach
    public void init() {
        reader = new DefaultResourceReader();
    }

    @Test
    public void givenInvalidResourcePath_whenRead_thenFail() throws IOException {
        String nullPath = null;
        String wrongPath = "/home/wrong/path/test";

        IllegalArgumentException nullPathException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> reader.setReadPath(nullPath));

        Assertions.assertEquals("invalid path was given", nullPathException.getMessage());

        IllegalArgumentException wrongPathExcption = Assertions.assertThrows(IllegalArgumentException.class,
                () -> reader.setReadPath(wrongPath));
        Assertions.assertEquals("path was incorrect or does not exist", wrongPathExcption.getMessage());
        String wrongDirectoryPath = Files.createTempDirectory("bank-transaction").toString();

        IllegalArgumentException wrongDirectoryException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> reader.setReadPath(wrongDirectoryPath));
        Assertions.assertEquals("path was not a file  ", wrongDirectoryException.getMessage());
    }

    @Test
    void givenEmptyFile_whenRead_thenFail() throws IOException {

        ResourceReader reader = new DefaultResourceReader();
        String path = createTempFile();
        IllegalArgumentException EmptyFile = Assertions.assertThrows(IllegalArgumentException.class,
                () -> reader.setReadPath(path));
        Assertions.assertEquals("the file was empty ", EmptyFile.getMessage());
    }

    public String createTempFile() throws IOException {
        Path path = Files.createTempFile("sample-file", ".txt");
        File file = path.toFile();
        file.deleteOnExit();
        return file.getAbsolutePath();
    }

}
