package com.progressoft.implment;

import com.progressoft.interfaces.Transaction;

import java.util.*;

public class DataMatcher {
    private List<Transaction> matchingTransaction;
    private List<Transaction> mismatchingTransaction;
    private List<Transaction> missingTransaction;

    public DataMatcher() {
        this.matchingTransaction = null;
        this.mismatchingTransaction = null;
        this.missingTransaction = null;
    }

    // TODO instead of returning a map with keys, you could introduce an object
    // describing the Result, name it
    // CompareResult {
    // matching
    // mismatching
    // missing
    //}
    public Map<String, List<Transaction>> getAllData(List<Transaction> transactionList1, List<Transaction> transactionList2) {
        isValid(transactionList1, transactionList2);
        matchingTransaction = matchingData(transactionList1, transactionList2);
        Map<String, List<Transaction>> temp = new HashMap<>();
        if (matchingTransaction.isEmpty() && missingTransaction.isEmpty() && mismatchingTransaction.isEmpty()) {
            throw new IllegalStateException("no data were found ");
        }
        temp.put("match-data", this.matchingTransaction);
        temp.put("mismatch-data", this.mismatchingTransaction);
        temp.put("missing-data", missingTransaction);
        return temp;
    }

    public int getMatchingCount() {
        return matchingTransaction.size() - 1;
    }

    public int getMissMatchCount() {
        return mismatchingTransaction.size() - 1;
    }

    public int getMissingCount() {
        return missingTransaction.size() - 1;
    }


    private void isValid(List<Transaction> transactionList1, List<Transaction> transactionList2) {
        if (transactionList1 == null || transactionList2 == null) {
            throw new IllegalArgumentException("invalid lists");
        }
        if (transactionList1.isEmpty() || transactionList2.isEmpty()) {
            throw new IllegalArgumentException("one or both lists are empty");
        }
    }

    private List<Transaction> matchingData(List<Transaction> transactionList1, List<Transaction> transactionList2) {
        List<Transaction> matchingTransactions = new ArrayList<>();
        List<Transaction> tempList1 = new ArrayList<>(transactionList1);
        List<Transaction> tempList2 = new ArrayList<>(transactionList2);
        for (Transaction transaction : transactionList1) {
            if (transactionList2.contains(transaction)) {
                matchingTransactions.add(transaction);
            }
        }

        tempList1.removeAll(matchingTransactions);
        tempList2.removeAll(matchingTransactions);
        missingAndMismatchData(tempList1, tempList2);
        return matchingTransactions;
    }

    private void missingAndMismatchData(List<Transaction> tempList1, List<Transaction> tempList2) {
        mismatchingTransaction = new ArrayList<>();
        for (Transaction transaction : tempList1) {
            for (Transaction value : tempList2) {
                if (isMissMatch(transaction, value)) {
                    mismatchingTransaction.add(transaction);
                    mismatchingTransaction.add(value);
                }
            }
        }
        tempList1.removeAll(mismatchingTransaction);
        tempList2.removeAll(mismatchingTransaction);
        missingTransaction = new ArrayList<>();
        missingTransaction.addAll(tempList1);
        missingTransaction.addAll(tempList2);

    }

    private boolean isMissMatch(Transaction t1, Transaction t2) {
        return t1.getReference().equals(t2.getReference());
    }

}
