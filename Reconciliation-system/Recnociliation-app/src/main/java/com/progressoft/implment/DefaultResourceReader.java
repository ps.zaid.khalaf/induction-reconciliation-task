package com.progressoft.implment;

import com.progressoft.interfaces.ResourceReader;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class DefaultResourceReader implements ResourceReader {
    private String path = null;

    public DefaultResourceReader() {
    }

    @Override
    public void setReadPath(String path) {
        isValid(path);
        this.path = path;
    }

    @Override
    public String getFilePath() {
        return path;
    }

    private void isValid(String path) {

        if (Objects.isNull(path)) {
            throw new IllegalArgumentException("invalid path was given");
        }
        Path testPath = Path.of(path);
        if (Files.notExists(testPath)) {
            throw new IllegalArgumentException("path was incorrect or does not exist");
        }
        if (Files.isDirectory(testPath)) {
            throw new IllegalArgumentException("path was not a file  ");
        }
        if (testPath.toFile().length() == 0)
            throw new IllegalArgumentException("the file was empty ");
    }
}

