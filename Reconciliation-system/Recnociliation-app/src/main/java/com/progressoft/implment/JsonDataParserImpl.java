package com.progressoft.implment;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.progressoft.interfaces.DataParser;
import com.progressoft.interfaces.ResourceReader;
import com.progressoft.interfaces.Transaction;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class JsonDataParserImpl implements DataParser {
    @Override
    public List<Transaction> parseData(ResourceReader file, FileOrigin origin) {

        try {

            ObjectMapper mapper = new ObjectMapper();
            List<Transaction> myObjects = mapper.readValue(new File(file.getFilePath()), new TypeReference<List<TransactionImpl>>() {
            });
            List<TransactionImpl> tempList = new ArrayList(Arrays.asList(myObjects.toArray()));
            for (TransactionImpl transaction : tempList) {
                transaction.setSource(origin.name());
                transaction.setDate(validateDate(transaction.getDate()));

            }

            if (myObjects.isEmpty()) {
                throw new IllegalArgumentException("invalid result");
            }
            return myObjects;
        } catch (IOException e) {

            throw new IllegalStateException("error occurred while parsing " + e.getCause() + " ");
        }
    }

    private String validateDate(String string) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.parse(string);
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");
            return newFormat.format(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e.getCause().getMessage());
        }
    }

}
