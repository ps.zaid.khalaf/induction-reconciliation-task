package com.progressoft.implment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.progressoft.interfaces.Transaction;

import java.math.BigDecimal;
import java.util.Objects;

@JsonIgnoreProperties(value = {"purpose"})
public class TransactionImpl implements Transaction {
    private String reference;
    private BigDecimal amount;
    private String CurrencyCode;
    private String date;
    private String source;

    public TransactionImpl setDate(String valueDate) {
        this.date = valueDate;
        return this;
    }

    public TransactionImpl setSource(String source) {
        this.source = source;
        return this;
    }

    @Override
    public String getSource() {
        return source;
    }

    public TransactionImpl setReference(String id) {
        this.reference = id;
        return this;
    }

    public TransactionImpl setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public TransactionImpl setCurrencyCode(String currencyCode) {
        this.CurrencyCode = currencyCode;
        return this;
    }

    @Override
    public String getReference() {
        return reference;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String getCurrencyCode() {
        return CurrencyCode;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        TransactionImpl that = (TransactionImpl) o;
        return Objects.equals(this.reference, that.getReference()) &&
                Objects.equals(this.amount, that.getAmount()) &&
                Objects.equals(this.CurrencyCode, that.getCurrencyCode()) &&
                Objects.equals(this.date, that.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference, amount, CurrencyCode, date);
    }

}
