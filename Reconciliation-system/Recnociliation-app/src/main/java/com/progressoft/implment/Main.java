package com.progressoft.implment;

import com.progressoft.interfaces.ResourceReader;
import com.progressoft.interfaces.Transaction;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<Transaction> sourceList = new ArrayList<>();

        List<Transaction> targetList = new ArrayList<>();

        CsvDataParserImpl csvDataParser = new CsvDataParserImpl();
        JsonDataParserImpl jsonDataParser = new JsonDataParserImpl();

        ResourceReader reader = new DefaultResourceReader();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter source file location:");
        String source = scanner.next();
        System.out.println("Enter source file format:");
        String fileType = scanner.next();
        reader.setReadPath(source);
        if (fileType.equals("CSV")) {
            sourceList.addAll(csvDataParser.parseData(reader, FileOrigin.SOURCE));
        } else if (fileType.equals("JSON")) {

            sourceList.addAll(jsonDataParser.parseData(reader, FileOrigin.SOURCE));
        }

        System.out.println("Enter target file location:");
        source = scanner.next();
        System.out.println("Enter target file format:");
        fileType = scanner.next();
        reader.setReadPath(source);

        if (fileType.equals("CSV")) {
            targetList.addAll(csvDataParser.parseData(reader, FileOrigin.TARGET));
        } else if (fileType.equals("JSON")) {
            targetList.addAll(jsonDataParser.parseData(reader, FileOrigin.TARGET));
        }


        DataMatcher matcher = new DataMatcher();
        Map<String, List<Transaction>> dataMap = matcher.getAllData(sourceList, targetList);

        DataWriter dataWriter = new DataWriter(null);
        dataWriter.writeAllDataToFiles(dataMap);


    }
}
