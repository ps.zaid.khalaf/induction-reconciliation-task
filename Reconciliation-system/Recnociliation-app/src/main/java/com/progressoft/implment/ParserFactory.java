package com.progressoft.implment;

import com.progressoft.interfaces.DataParser;

public class ParserFactory {

    public static DataParser getParser(String type) {

        DataParser parser = null;
        if (type.toUpperCase().equalsIgnoreCase("CSV")) {
            parser = new CsvDataParserImpl();
        } else if (type.toUpperCase().equalsIgnoreCase("JSON")) {
            parser = new JsonDataParserImpl();
        }
        return parser;
    }
}
