package com.progressoft.implment;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.progressoft.interfaces.Transaction;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class DataWriter {

    private static final String[] MATCH_HEADER_INFO = new String[]{"reference", "amount", "currencyCode", "date \n"};
    private static final String[] MISMATCHING_AND_MISSING_HEADER_INFO = new String[]{"source", "reference", "amount", "currencyCode", "date \n"};

    private static final String FILE_MATCH_HEADER = "transaction id" + "," + "amount" + "," + "currecny code" + "," + "value date \n";
    private static final String FILE_MISSING_AND_MISMATCH_HEADER = "found in file" + "," + "transaction id" + "," + "amount" + "," + "currecny code" + "," + "value date \n";

    private static final String MATCHING_FILE_NAME = "matching-transactions.csv";
    private static final String MISMATCHED_FILE_NAME = "mismatched-transactions.csv";
    private static final String MISSING_FILE_NAME = "missing-transactions.csv";
    private String fileDestination;
    private String folderFullPath;

    public void writeAllDataToFiles(Map<String, List<Transaction>> dataMap) {
        isValid(dataMap);
        writeDataToFile(dataMap.get("match-data"), MATCH_HEADER_INFO, MATCHING_FILE_NAME, FILE_MATCH_HEADER);
        writeDataToFile(dataMap.get("mismatch-data"), MISMATCHING_AND_MISSING_HEADER_INFO, MISMATCHED_FILE_NAME, FILE_MISSING_AND_MISMATCH_HEADER);
        writeDataToFile(dataMap.get("missing-data"), MISMATCHING_AND_MISSING_HEADER_INFO, MISSING_FILE_NAME, FILE_MISSING_AND_MISMATCH_HEADER);
    }


    public DataWriter(String fileDestination) {
        folderFullPath = null;
        this.fileDestination = fileDestination;
    }

    public void setFolderName(String folderName) {
        folderFullPath = null;
        folderFullPath = fileDestination + "/" + folderName;
    }

    private void writeDataToFile(List<Transaction> matching, String[] header, String fileName, String fileHeaderString) {
        String resourcePath = fileCreator(fileName);
        try (FileWriter writer = new FileWriter(resourcePath)) {

            writer.write(fileHeaderString);
            // column name in order
            ColumnPositionMappingStrategy<Transaction> mappingStrategy =
                    new ColumnPositionMappingStrategy<>();
            mappingStrategy.setType(TransactionImpl.class);
            // Arrange column name as provided in below array.
            mappingStrategy.setColumnMapping(header);
            // Createing StatefulBeanToCsv object
            StatefulBeanToCsvBuilder<Transaction> builder =
                    new StatefulBeanToCsvBuilder<>(writer);
            StatefulBeanToCsv<Transaction> beanWriter =
                    builder.withMappingStrategy(mappingStrategy).build();
            // Write list to StatefulBeanToCsv object
            beanWriter.write(matching);
            // closing the writer object

        } catch (CsvRequiredFieldEmptyException | CsvDataTypeMismatchException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private String fileCreator(String fileName) {
        String path = destDirectory();
        try {
            Path newFilePath = Paths.get(path + "/" + fileName);
            Files.deleteIfExists(newFilePath);
            Files.createFile(newFilePath);
        } catch (IOException e) {
            throw new IllegalArgumentException("error " + e.getCause());
        }
        return path + "/" + fileName;
    }

    private void isValid(Map<String, List<Transaction>> matcher) {
        if (matcher == null) {
            throw new IllegalStateException("invalid map");
        }
        if (matcher.isEmpty()) {
            throw new IllegalStateException("map was empty");
        }
    }

    private String destDirectory() {
        String dir = null;
        File file;
        if (fileDestination == null) {
            String d = System.getProperty("user.home");
            dir = d + File.separator + "Documents" + File.separator + "Reconciliation-system results";
            file = new File(dir);
            System.out.println(file.mkdirs());
        } else {
            dir = "./" + folderFullPath;
            file = new File(dir);
            System.out.println(file.mkdir());
        }
        return dir;
    }
}
