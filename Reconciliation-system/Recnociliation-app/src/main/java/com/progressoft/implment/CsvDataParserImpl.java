package com.progressoft.implment;

import com.progressoft.interfaces.DataParser;
import com.progressoft.interfaces.ResourceReader;
import com.progressoft.interfaces.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CsvDataParserImpl implements DataParser {

    @Override
    public List<Transaction> parseData(ResourceReader reader, FileOrigin origin) {
        if (reader == null || reader.getFilePath().equals("")) {
            throw new IllegalArgumentException("invalid reader was inserted or file path was empty");
        }
        List<Transaction> result = new ArrayList<>();
        try (BufferedReader bufferedReader = Files.newBufferedReader(Path.of(reader.getFilePath()))) {
            bufferedReader.readLine();
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] strings = line.replaceAll(" ", "").split(",");
                addRecord(origin, result, strings);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalStateException("an error accord when reading from file :" + e.getCause().getMessage());
        }
        return result;
    }

    private void addRecord(FileOrigin origin, List<Transaction> result, String[] strings) {
        Currency currency = Currency.getInstance(strings[3]);
        BigDecimal amount = new BigDecimal(strings[2]).setScale(currency.getDefaultFractionDigits(), RoundingMode.HALF_UP);
        TransactionImpl transaction = new TransactionImpl()
                .setReference(strings[0])
                .setAmount(amount)
                .setCurrencyCode(strings[3])
                .setSource(origin.name())
                .setDate(validateDate(strings[5]));
        result.add(transaction);
    }

    private String validateDate(String string) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(string);
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");
            return newFormat.format(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e.getCause().getMessage());
        }
    }

}
