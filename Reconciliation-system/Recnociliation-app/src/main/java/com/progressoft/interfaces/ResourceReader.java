package com.progressoft.interfaces;

public interface ResourceReader {
    void setReadPath(String o);

    String getFilePath();

}
