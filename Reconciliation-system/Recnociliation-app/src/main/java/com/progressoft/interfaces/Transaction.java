package com.progressoft.interfaces;

import java.math.BigDecimal;

public interface Transaction {
    String getReference();

    BigDecimal getAmount();

    String getCurrencyCode();

    String getDate();

    String getSource();
}
