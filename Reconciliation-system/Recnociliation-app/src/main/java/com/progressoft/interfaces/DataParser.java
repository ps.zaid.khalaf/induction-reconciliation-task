package com.progressoft.interfaces;

import java.util.List;

import com.progressoft.implment.FileOrigin;

public interface DataParser {

    List<Transaction> parseData(ResourceReader resourceReader, FileOrigin origin);

}
