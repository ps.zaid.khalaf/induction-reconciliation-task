<%--
  Created by IntelliJ IDEA.
  User: zaid
  Date: ٨‏/٩‏/٢٠٢٠
  Time: ٣:١٠ م
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="recon" tagdir="/WEB-INF/tags" %>
<style type="text/css">
    body {
        background: aliceblue !important;
    }

    thead {
        background: #5286F3 !important;
    }
</style>
<html lang="en">
<head>
    <title>results</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>data found </h2>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">matching</a></li>
        <li><a data-toggle="tab" href="#menu1">miss match</a></li>
        <li><a data-toggle="tab" href="#menu2">missing </a></li>

    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <h3>matching data</h3>
            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Transaction Id</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Currency code</th>
                    <th scope="col">ValueDate</th>
                </tr>
                </thead>
                <tbody>
                <c:set var="count" value="0"/>
                <c:forEach items="${requestScope.matching}" var="transaction">
                    <tr>
                        <c:set var="count" value="${count + 1}"/>
                        <td>${count}</td>
                        <td>${transaction.reference}</td>
                        <td>${transaction.amount}</td>
                        <td>${transaction.currencyCode}</td>
                        <td>${transaction.date}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div id="menu1" class="tab-pane fade">
            <h3>miss match</h3>

            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Transaction Id</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Currency code</th>
                    <th scope="col">ValueDate</th>
                </tr>
                </thead>
                <tbody>
                <c:set var="count" value="0"/>
                <c:forEach items="${requestScope.mismatching}" var="transaction">
                    <tr>
                        <c:set var="count" value="${count + 1}"/>
                        <td>${count}</td>
                        <td>${transaction.reference}</td>
                        <td>${transaction.amount}</td>
                        <td>${transaction.currencyCode}</td>
                        <td>${transaction.date}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div id="menu2" class="tab-pane fade">
            <h3>missing</h3>

            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Transaction Id</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Currency code</th>
                    <th scope="col">ValueDate</th>
                </tr>
                </thead>
                <tbody>
                <c:set var="count" value="0"/>
                <c:forEach items="${requestScope.missing}" var="transaction">
                    <tr>
                        <c:set var="count" value="${count + 1}"/>
                        <td>${count}</td>
                        <td>${transaction.reference}</td>
                        <td>${transaction.amount}</td>
                        <td>${transaction.currencyCode}</td>
                        <td>${transaction.date}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<button type="button" class="btn btn-primary" onclick="toDownload()">download file</button>
<br>
<button type="button" class="btn btn-outline-primary" onclick="toSourceUploader()">compare new files</button>
</body>
<script> function toSourceUploader() {
    window.location = "${pageContext.request.contextPath}/upload";
}

function toDownload() {
    window.location = "${pageContext.request.contextPath}/download";
}

</script>
<br>
<br>
<recon:logout/>

</html>