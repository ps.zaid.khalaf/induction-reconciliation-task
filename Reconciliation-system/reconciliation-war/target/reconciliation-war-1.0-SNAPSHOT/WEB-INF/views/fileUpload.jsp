<%--
  Created by IntelliJ IDEA.
  User: zaid
  Date: ٧‏/٩‏/٢٠٢٠
  Time: ٨:٢٢ م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="recon" tagdir="/WEB-INF/tags" %>

<html>
<head>

    <title>source file uploader</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        body {
            background: #f0f8ff !important;
        }
    </style>

</head>
<body>
<form class="md-form" id="upload-form" action="${pageContext.request.contextPath}/uploader" method="post"
      enctype="multipart/form-data">
    <br>
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Choose file</span>
            <input type="file" class="form-control" required="required" name="sourceFile">
        </div>
        <br>
        <br>
        <br>
        <br>
        <p> enter file name</p>
        <div class="file-path-wrapper">
            <input class="file-path validate" required="required" name="sourcefile" type="text"
                   placeholder="Upload your file">
        </div>
    </div>
    <br>
    <div>
        <select name="sourcetype">
            <option value="csv">CSV</option>
            <option value="json">JSON</option>
        </select>
        <button id="upload-btn" class="btn btn-primary" type="submit" value="Upload">upload</button>
    </div>
    <br>
    <br>
    <recon:logout />
</form>
</body>
</html>

<!-- <p>source name
<label>
<input type="text" name="sourcefile"/>
</label></p>
<br>
Select File to Upload:<input type="file" name="sourceFile">
<br> -->