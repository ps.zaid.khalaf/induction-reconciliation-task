package com.progressoft.reconciliation;

import org.junit.jupiter.api.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;


class UserRegistrationServletTest {

    @Test
    public void givenValidRequest_whenDoGet_thenSuccess() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher("/WEB-INF/views/fileUpload.jsp")).thenReturn(requestDispatcher);
        new FileUploaderServlet().doGet(request, response);
        verify(requestDispatcher).forward(request, response);

    }
}