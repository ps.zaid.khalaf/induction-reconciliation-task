<%--
  Created by IntelliJ IDEA.
  User: zaid
  Date: ١٤‏/٩‏/٢٠٢٠
  Time: ٣:٥٠ م
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="recon" tagdir="/WEB-INF/tags" %>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>activity</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <h3>matching data</h3>
        <table class="table table-dark">
            <tbody>
            <c:set var="count" value="0"/>
            <c:forEach items="${requestScope.activityList}" var="transaction">
                <tr>
                    <c:set var="count" value="${count + 1}"/>
                    <td>${count}</td>
                    <td>${transaction.time}</td>
                    <td>${transaction.sourceFile}</td>
                    <td>${transaction.targetFile}</td>
                    <td>${transaction.matchCount}</td>
                    <td>${transaction.missMatchCount}</td>
                    <td>${transaction.missingCount}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<recon:logout/>
</body>
</html>