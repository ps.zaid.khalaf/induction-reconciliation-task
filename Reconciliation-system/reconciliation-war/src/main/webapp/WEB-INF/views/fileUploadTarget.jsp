<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: zaid
  Date: ٨‏/٩‏/٢٠٢٠
  Time: ١١:٤٨ ص
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="recon" tagdir="/WEB-INF/tags" %>
<html>
<head>
    <title>target file uploader page </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        body {
            background: aliceblue !important;
        }


    </style>
</head>
<body>

<form id="upload-form" action="${pageContext.request.contextPath}/targetUploader" method="post"
      enctype="multipart/form-data">
    <br>
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Choose file</span>
            <input type="file" class="form-control" name="targetFile" required="required">
        </div>
        <br>
        <br>
        <br>
        <br>
        <p> enter file name</p>
        <div class="file-path-wrapper">
            <input class="file-path validate" required="required" name="targetname" type="text"
                   placeholder="Upload your file">
        </div>
    </div>
    <br>
    <label>
        <select name="targetype">
            <option value="csv">CSV</option>
            <option value="json">JSON</option>
        </select>
    </label>
    <button id="upload-btn" class="btn btn-primary" type="submit" value="Upload">upload</button>
    <br>
    <br>
    <button type="submit" class="btn btn-outline-primary" onclick="toSourceUploader()">back</button>

</form>
<script> function toSourceUploader() {
    window.location = "${pageContext.request.contextPath}/uploader";
}
</script>

<br>
<br>
<recon:logout/>
</body>
</html>
<!-- <p>target name Select File to Upload:<input type="file" name="targetFile"/>
<label>
<input type="text" name="targetname"/>
</label></p> -->