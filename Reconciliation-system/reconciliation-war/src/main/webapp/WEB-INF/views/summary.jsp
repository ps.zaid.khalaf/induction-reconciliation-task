<%--
  Created by IntelliJ IDEA.
  User: zaid
  Date: ٩‏/٩‏/٢٠٢٠
  Time: ١٢:١٩ م
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="recon" tagdir="/WEB-INF/tags" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <title>summary</title>
    <style>
        .bs-example {
            margin: 20px;
        }
    </style>
</head>
<body>
<div class="bs-example">
    <div class="container-fluid">
        <div class="row">
            <div class="card-deck">
                <div class="card bg-primary text-white">
                    <div class="card-body">
                        <h4 class="card-header">source</h4>
                        <p class="card-text"><h4>file name :<c:out value="${sessionScope.sourcefile}"/></h4></p>
                        <p class="card-text"><h4>file type :<c:out value="${sessionScope.sourcetype}"/></h4></p>
                    </div>
                </div>
                <div class="card bg-warning text-white">
                    <div class="card-body">
                        <h4 class="card-header">Target</h4>
                        <p class="card-text"><h4> file name :<c:out value="${sessionScope.targetname}"/></h4></p>
                        <p class="card-text"><h4>file type:<c:out value="${sessionScope.targetype}"/></h4></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</form>
<form action="${pageContext.request.contextPath}/uploader" class="btn btn-outline-warning" method="get">
    <input type="submit" value="cancel">
</form>
<form action="${pageContext.request.contextPath}/compare" method="post">
    <input type="submit" class="btn btn-outline-primary" value="compare">
</form>

<button type="submit" class="btn btn-outline-primary" onclick="toSourceUploader()">back</button>
</form>
<script> function toSourceUploader() {
    window.location = "${pageContext.request.contextPath}/targetUploader";
}
</script>
<recon:logout/>
</body>
</html>