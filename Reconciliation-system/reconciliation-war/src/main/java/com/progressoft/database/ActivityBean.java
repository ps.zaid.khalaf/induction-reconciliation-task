package com.progressoft.database;

public class ActivityBean {
    private String userName;
    private String time;
    private String sourceFile;
    private String targetFile;
    private int matchCount;
    private int missMatchCount;
    private int missingCount;

    public ActivityBean() {
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }

    public void setTargetFile(String targetFile) {
        this.targetFile = targetFile;
    }

    public void setMatchCount(int matchCount) {
        this.matchCount = matchCount;
    }

    public void setMissMatchCount(int missMatchCount) {
        this.missMatchCount = missMatchCount;
    }

    public void setMissingCount(int missingCount) {
        this.missingCount = missingCount;
    }

    public String getUserName() {
        return userName;
    }

    public String getTime() {
        return time;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public String getTargetFile() {
        return targetFile;
    }

    public int getMatchCount() {
        return matchCount;
    }

    public int getMissMatchCount() {
        return missMatchCount;
    }

    public int getMissingCount() {
        return missingCount;
    }

    @Override
    public String toString() {
        return "ActivityBean{" +
                "time='" + time + '\'' +
                ", sourceFile='" + sourceFile + '\'' +
                ", targetFile='" + targetFile + '\'' +
                ", matchCount=" + matchCount +
                ", missMatchCount=" + missMatchCount +
                ", missingCount=" + missingCount +
                '}';
    }
}
