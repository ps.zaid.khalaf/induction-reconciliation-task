package com.progressoft.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ActivityDao {


    private final String connectionString;
    private final String user;
    private final String password;

    public ActivityDao(String connectionString, String user, String password) {
        this.connectionString = connectionString;
        this.user = user;
        this.password = password;
    }

    public boolean addActivity(ActivityBean activityBean) {
        boolean status = false;
        String query = " insert into usr_actvt (time,user_name ,s_file," +
                "t_file," +
                "match_rec," +
                "missmatch_rec," +
                "missing_rec)" + " values (?,?,?,?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(connectionString, user, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, activityBean.getTime());
            preparedStatement.setString(2, activityBean.getUserName());
            preparedStatement.setString(3, activityBean.getSourceFile());
            preparedStatement.setString(4, activityBean.getTargetFile());
            preparedStatement.setInt(5, activityBean.getMatchCount());
            preparedStatement.setInt(6, activityBean.getMissMatchCount());
            preparedStatement.setInt(7, activityBean.getMissingCount());
            status = preparedStatement.execute();
        } catch (SQLException throwables) {
            printSQLException(throwables);
        }
        return status;
    }

    public List<ActivityBean> getAllActivityByName(String name) {
        String query = " select * from usr_actvt where user_name=?";
        List<ActivityBean> activityBeans = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(connectionString, user, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, name);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ActivityBean bean = new ActivityBean();
                bean.setTime(rs.getString("user_name"));
                bean.setTime(rs.getString("time"));
                bean.setSourceFile(rs.getString("s_file"));
                bean.setTargetFile(rs.getString("t_file"));
                bean.setMatchCount(rs.getInt("match_rec"));
                bean.setMissMatchCount(rs.getInt("missmatch_rec"));
                bean.setMissingCount(rs.getInt("missing_rec"));
                activityBeans.add(bean);
            }
        } catch (
                SQLException e) {
            printSQLException(e);
        }


        return activityBeans;
    }
    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}
