package com.progressoft.database;

import java.sql.*;

public class UserDao implements IUserDao {
    private final String connectionString;
    private final String user;
    private final String password;

    public UserDao(String connString, String user, String password) {
        this.connectionString = connString;
        this.user = user;
        this.password = password;
    }

    @Override
    public boolean validateUser(UserBean loginBean) throws SQLException {
        String query = " select name,password from users where name=?";
        String hashedPassword = null;
        boolean status = false;
        try (Connection connection = DriverManager.getConnection(connectionString, user, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, loginBean.getName());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                hashedPassword = rs.getString("password");
            }
            if (hashedPassword == null) {
                throw new IllegalStateException("no password found ");
            }
            status = PasswordEncryptor.checkPassword(loginBean.getPassWord(), hashedPassword);
        } catch (
                SQLException e) {
            printSQLException(e);
        }
        return status;
    }

    @Override
    public boolean addUser(UserBean loginBean) {
        boolean status = false;
        String query = " insert into users (name, password)" + " values (?,?)";
        try (Connection connection = DriverManager.getConnection(connectionString, "root", "")) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, loginBean.getName());
            preparedStatement.setString(2, PasswordEncryptor.hashPassword(loginBean.getPassWord()));
            status = preparedStatement.execute();
        } catch (SQLException throwables) {
            printSQLException(throwables);
        }
        return status;
    }

    public boolean isEmpty() {
        boolean status = false;
        String query = " select count(*) from users";
        try (Connection connection = DriverManager.getConnection(connectionString, "root", "")) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeQuery();
            ResultSet resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                if (resultSet.getInt(1) == 0) {
                    status = true;
                }
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return status;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


}

