package com.progressoft.database;

import org.mindrot.jbcrypt.BCrypt;

import java.sql.SQLException;

// TODO .NET convention, we don't use I for our interfaces
public interface IUserDao {
    boolean validateUser(UserBean loginBean) throws SQLException;

    boolean addUser(UserBean loginBean);


    class PasswordEncryptor {
        static String hashPassword(String plainTextPassword) {
            return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
        }

        static boolean checkPassword(String plainPassword, String hashedPassword) {
            return BCrypt.checkpw(plainPassword, hashedPassword);

        }
    }

    boolean isEmpty();
}
