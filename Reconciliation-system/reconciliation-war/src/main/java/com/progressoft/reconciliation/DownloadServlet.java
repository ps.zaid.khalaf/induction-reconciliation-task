package com.progressoft.reconciliation;

import com.progressoft.database.ActivityBean;
import com.progressoft.database.ActivityDao;
import com.progressoft.util.FilesUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DownloadServlet extends HttpServlet {
    private final ActivityDao activityDao;

    public DownloadServlet(ActivityDao activityDao) {
        this.activityDao = activityDao;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String uFile = String.valueOf(session.getAttribute("username"));
        String zipFilePath = FilesUtils.zipFolder("/target/tmp/" + uFile);
        response.setContentType("APPLICATION/OCTET-STREAM");
        PrintWriter out = response.getWriter();
        String filename = "result.zip";
        response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        FileInputStream fl = new FileInputStream(zipFilePath);
        int i;
        while ((i = fl.read()) != -1) {

            out.write(i);

        }
        fl.close();

        out.close();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println(formatter.format(date));
        String sourcePath = (String) session.getAttribute("sourcefile");
        String targetPath = (String) session.getAttribute("targetname");
        int matchCount = (int) session.getAttribute("matchCount");
        int missMatchCount = (int) session.getAttribute("missMatchCount");
        int missingCount = (int) session.getAttribute("missMatchCount");
        ActivityBean bean = new ActivityBean();
        bean.setUserName(session.getAttribute("username").toString());
        bean.setTime(formatter.format(date));
        bean.setSourceFile(sourcePath);
        bean.setTargetFile(targetPath);
        bean.setMatchCount(matchCount);
        bean.setMissMatchCount(missMatchCount);
        bean.setMissingCount(missingCount);
        System.out.println(bean);
        // TODO this is the only add activity introduce, you should add activity on comparing also
        activityDao.addActivity(bean);

    }
}

