package com.progressoft.reconciliation;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUploaderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/views/fileUpload.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String type = request.getParameter("sourcetype");
        String file = request.getParameter("sourcefile");
        Part sourceFile = request.getPart("sourceFile");
        HttpSession session = request.getSession();
        String fileName = session.getAttribute("username") + "-" + file + "." + type;
        sourceFile.write(fileName);
        // TODO static path
        Path path = Paths.get("target/tmp/source", fileName);
        session.setAttribute("sourcePath", path.toString());
        session.setAttribute("sourcefile", file);
        session.setAttribute("sourcetype", type);
        request.getRequestDispatcher("/WEB-INF/views/fileUploadTarget.jsp").forward(request, response);

    }
}
