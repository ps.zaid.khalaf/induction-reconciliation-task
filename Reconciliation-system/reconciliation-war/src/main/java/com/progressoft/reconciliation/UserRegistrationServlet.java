package com.progressoft.reconciliation;


import com.progressoft.database.IUserDao;
import com.progressoft.database.UserBean;
import com.progressoft.database.UserDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;


public class UserRegistrationServlet extends HttpServlet {
    private IUserDao loginDao;

    public UserRegistrationServlet(IUserDao loginDao) {
        this.loginDao = loginDao;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // TODO how this would work
        resp.sendRedirect("/WEB-INF/views/logIn.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        UserBean loginBean = new UserBean();
        loginBean.setName(username);
        loginBean.setPassWord(password);

        try {
            if (loginDao.validateUser(loginBean)) {
                HttpSession session = req.getSession();
                session.setAttribute("username", username);
                RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/fileUpload.jsp");
                dispatcher.forward(req, resp);
            } else {

                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "no user found" + username);
            }
        } catch (SQLException throwable) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "something went wrong " + throwable.getCause());
        }
    }
}

