package com.progressoft.reconciliation;

import com.progressoft.database.ActivityDao;
import com.progressoft.database.IUserDao;
import com.progressoft.database.UserDao;
import com.progressoft.implment.DataWriter;

import javax.servlet.*;
import java.util.Set;

public class ServletInitializer implements ServletContainerInitializer {


    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        // TODO static connection string
        String connectionString = "jdbc:mysql://localhost:3306/recon_db?serverTimezone=UTC &useSSL=false";
        String user = "root";
        String pass = "";
        registerUserRegistrationServlet(ctx, setUpUserDao(connectionString, user, pass));

        registerLogOutServlet(ctx);
        registerFileUploaderServlet(ctx);
        registerFileUploaderTargetServlet(ctx);
        registerCompareServlet(ctx);
        registerDownloadServlet(ctx, setUpActivityDao(connectionString, user, pass));
        registerActivityServlet(ctx, setUpActivityDao(connectionString, user, pass));
    }

    private void registerUserRegistrationServlet(ServletContext ctx, IUserDao iUserDao) {
        UserRegistrationServlet userRegistrationServlet = new UserRegistrationServlet(iUserDao);
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("userRegistrationServlet", userRegistrationServlet);
        servletRegistration.addMapping("/register");
    }

    private void registerLogOutServlet(ServletContext ctx) {
        UserLogOutServlet userLogOutServlet = new UserLogOutServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("userLogOutServlet", userLogOutServlet);
        servletRegistration.addMapping("/logout");
    }

    private void registerFileUploaderServlet(ServletContext ctx) {

        FileUploaderServlet uploaderServlet = new FileUploaderServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet(" uploaderServlet", uploaderServlet);
        servletRegistration.setMultipartConfig(
                new MultipartConfigElement
                        ("source", 1024 * 1024 * 50,
                                1024 * 1024 * 50, 1024 * 1024));
        servletRegistration.addMapping("/uploader");
    }

    private void registerFileUploaderTargetServlet(ServletContext ctx) {
        FileUploaderTargetServlet targetServlet = new FileUploaderTargetServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet(" uploaderTargetServlet", targetServlet);
        servletRegistration.setMultipartConfig(
                new MultipartConfigElement
                        ("target", 1024 * 1024 * 50,
                                1024 * 1024 * 50, 1024 * 1024));
        servletRegistration.addMapping("/targetUploader");
    }

    private void registerCompareServlet(ServletContext ctx) {

        String resultPath = "/target/tmp";
        CompareServlet compareServlet = new CompareServlet(setResultPath(resultPath));
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("compareServlet", compareServlet);
        servletRegistration.addMapping("/compare");
    }

    private void registerDownloadServlet(ServletContext ctx, ActivityDao activityDao) {
        DownloadServlet downloadServlet = new DownloadServlet(activityDao);
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("downloadServlet", downloadServlet);
        servletRegistration.addMapping("/download");
    }

    private void registerActivityServlet(ServletContext ctx, ActivityDao activityDao) {
        ActivityServlet activityServlet = new ActivityServlet(activityDao);
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("activityServlet", activityServlet);
        servletRegistration.addMapping("/activity");
    }

    private DataWriter setResultPath(String path) {
        return new DataWriter(path);
    }

    private IUserDao setUpUserDao(String connectionString, String user, String password) {
        return new UserDao(connectionString, user, password);
    }

    private ActivityDao setUpActivityDao(String conn, String user, String pass) {
        return new ActivityDao(conn, user, pass);

    }

}
