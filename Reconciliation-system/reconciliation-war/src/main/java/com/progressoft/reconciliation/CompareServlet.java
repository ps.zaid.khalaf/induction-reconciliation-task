package com.progressoft.reconciliation;

import com.progressoft.implment.*;
import com.progressoft.interfaces.DataParser;
import com.progressoft.interfaces.ResourceReader;
import com.progressoft.interfaces.Transaction;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class CompareServlet extends HttpServlet {
    private DataMatcher matcher;
    private final DataWriter dataWriter;

    @Override
    public void init() {
        matcher = new DataMatcher();
    }

    public CompareServlet(DataWriter dataWriter) {

        this.dataWriter = dataWriter;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    // TODO long method
        List<Transaction> sourceTransactionList;
        List<Transaction> targetTransactionList;
        HttpSession session = req.getSession();

        String sourceFile = (String) session.getAttribute("sourcePath");
        String targetFile = (String) session.getAttribute("targetPath");
        String sourceType = (String) session.getAttribute("sourcetype");
        String targetType = (String) session.getAttribute("targetype");
        DataParser sourceParser = ParserFactory.getParser(sourceType);
        DataParser targetParser = ParserFactory.getParser(targetType);
        ResourceReader reader = new DefaultResourceReader();
        reader.setReadPath(sourceFile);
        sourceTransactionList = sourceParser.parseData(reader, FileOrigin.SOURCE);
        reader.setReadPath(targetFile);
        targetTransactionList = targetParser.parseData(reader, FileOrigin.TARGET);
        Map<String, List<Transaction>> allData = matcher.getAllData(sourceTransactionList, targetTransactionList);
        String username = String.valueOf(session.getAttribute("username"));
        dataWriter.setFolderName(username);
        dataWriter.writeAllDataToFiles(allData);
        req.setAttribute("matching", allData.get("match-data"));
        req.setAttribute("mismatching", allData.get("mismatch-data"));
        req.setAttribute("missing", allData.get("missing-data"));
        session.setAttribute("matchCount", matcher.getMatchingCount());
        session.setAttribute("missMatchCount", matcher.getMissMatchCount());
        session.setAttribute("missingCount", matcher.getMissingCount());
        req.getRequestDispatcher("/WEB-INF/views/result.jsp").forward(req, resp);
    }

}
