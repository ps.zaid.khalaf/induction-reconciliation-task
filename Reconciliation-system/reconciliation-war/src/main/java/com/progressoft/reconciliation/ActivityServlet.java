package com.progressoft.reconciliation;

import com.progressoft.database.ActivityDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ActivityServlet extends HttpServlet {
    ActivityDao activityDao;

    public ActivityServlet(ActivityDao activityDao) {
        this.activityDao = activityDao;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String username = String.valueOf(session.getAttribute("username"));
        req.setAttribute("activityList", activityDao.getAllActivityByName(username));
        req.getRequestDispatcher("/WEB-INF/views/activity.jsp").forward(req, resp);
    }
}
