package com.progressoft.reconciliation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUploaderTargetServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       resp.sendRedirect("/WEB-INF/views/fileUploadTarget.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// TODO duplicate code with FileUploader
        String type = request.getParameter("targetype");
        String file = request.getParameter("targetname");
        Part targetFile = request.getPart("targetFile");
        HttpSession session = request.getSession();
        String fileName = session.getAttribute("username") + "-" + file + "." + type;
        targetFile.write(fileName);
        Path path = Paths.get("target/tmp/target", fileName);
        System.out.println(path.toString());

        session.setAttribute("targetPath", path.toString());
        session.setAttribute("targetype", type);
        session.setAttribute("targetname", file);
        getServletContext().getRequestDispatcher("/WEB-INF/views/summary.jsp").forward(
                request, response);
    }

}
