package com.progressoft.util;

import com.progressoft.database.IUserDao;
import com.progressoft.database.UserBean;

import java.sql.SQLException;

public class DataBaseUsersLoader {
    public static void populateDataBase(IUserDao dao) {
        UserBean uBean1 = new UserBean();
        UserBean uBean2 = new UserBean();
        uBean1.setName("zaid");
        uBean1.setPassWord("12345");
        uBean2.setName("ahmad");
        uBean2.setPassWord("123");
        try {
            if (dao.isEmpty()) {
                System.out.println("user name:zaid  added !!!");
                dao.addUser(uBean1);
                System.out.println("user2 name:ahmad added !!!");
                dao.addUser(uBean2);
            } else {
                System.out.println("already inserted !!!");
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("some thing went wrong " + e.getCause());
        }
    }

}
