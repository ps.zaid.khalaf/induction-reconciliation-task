package com.progressoft.util;

import org.zeroturnaround.zip.ZipUtil;

import java.io.File;

public class FilesUtils {
    public static String getAbsolutePath() {
        return new File("").getAbsolutePath();
    }
    public static String zipFolder(String path) {
        String zipPath = getAbsolutePath() + File.separator + path;
        ZipUtil.pack(new File(getAbsolutePath() + path), new File(getAbsolutePath() + path + ".zip"));
        return zipPath + ".zip";
    }

}




